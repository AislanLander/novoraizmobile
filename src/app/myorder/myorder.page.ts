import { Component, OnInit } from '@angular/core';
import { LoadingController, Events } from '@ionic/angular';
import { OrdersProvider } from '../../providers/orders';
import { CurrenciesProvider } from '../../providers/currencies';
import { Storage } from '@ionic/storage';
import { ProductsProvider } from 'src/providers/products';

@Component({
	selector: 'app-myorder',
	templateUrl: './myorder.page.html',
	styleUrls: ['./myorder.page.scss'],
})
export class MyorderPage implements OnInit {
	panelOpenState = false;
	list_orders_success:any = [];
	list_orders_pending:any = [];
	list_listas:any;
	settings: any;

	user_id: any;
	list_product: any;
	start: any;

	loading: any;
	segment = 'pedidos';
	list_cart: Array<any>;

	constructor(public events: Events,
		public currenciesProv: CurrenciesProvider, 
		public ordersProv: OrdersProvider, 
		public storage: Storage,
		public loadingCtrl: LoadingController,
		public productsProv: ProductsProvider
	) { 
		this.presentLoading();
		this.events.subscribe('cart_list: change', (lst) => {
			this.list_cart = lst;
		});

		this.storage.get('setting').then(data=>{
			this.settings=data;
		})

		this.storage.get('user').then((obj) => {
			console.log(obj);
			if (obj == null) {
				this.user_id = null;
			}else{
				this.user_id = obj.id_auth;
			
				this.ordersProv.getOrdersPending(this.user_id).then(data => {
					this.list_orders_pending = data;
					for (var i = 0; i < data.length; ++i) {
						this.list_orders_pending[i]['lst_items'] = JSON.parse(data[i].payload.doc.data().items);
					}
					console.log(this.list_orders_pending);
				});
				this.ordersProv.getListas(obj.email).then(data =>{
					this.list_listas = data.map(e => {
						return {
							id: e.payload.doc.id,
							...e.payload.doc.data()
						  };
					});
					console.log(this.list_listas);
				});
				this.ordersProv.getOrdersSuccess(this.user_id, 10).then(data => {
					this.loading.dismiss().then(() => {
						this.list_orders_success = data;
						for (var i = 0; i < data.length; ++i) {
							this.list_orders_success[i]['lst_items'] = JSON.parse(data[i].payload.doc.data().items);
						}
					})
					console.log(this.list_orders_success);
				});
			}
		});

	}

	async presentLoading() {
		this.loading = await this.loadingCtrl.create({
			message: 'aguarde',
			duration: 2000
		});
		return await this.loading.present();
	}

	ionViewWillEnter(){
		this.storage.ready().then(() => {
			this.storage.get('cart_list').then((val) => {
				if(!val || val == null){
					this.list_cart = new Array();
				}else{
					this.list_cart = val;
				}
				console.log(this.list_cart);
			});
		});
	}
	adicionaRecomendacao(recomendacao) {
		this.productsProv.getProduct(null, 12).then(data => {
			this.list_product = data;
			this.start = data[data.length - 1].payload.doc.data().name;
			console.log(this.list_product);
			recomendacao.forEach(element1 => {
				this.list_product.forEach(item => {
					if(element1.name == item.payload.doc.data().name){
															
						console.log(item);

						let itemCv = {
							id: item.payload.doc.id,
							name: item.payload.doc.data().name,
							price: item.payload.doc.data().price,
							discount: item.payload.doc.data().discount,
							description: item.payload.doc.data().description,
							vote: item.payload.doc.data().vote,
							created: item.payload.doc.data().created,
							id_cat: item.payload.doc.data().id_cat,
							tag: item.payload.doc.data().tag,
							thumb: item.payload.doc.data().thumb,
							thumb1: item.payload.doc.data().thumb1,
							thumb2: item.payload.doc.data().thumb2,
							thumb3: item.payload.doc.data().thumb3,
							thumb4: item.payload.doc.data().thumb4,
							quantity: 1
						}

						let temp = this.list_cart.filter((element) => {
							if(element.id == itemCv.id){
								element.quantity = 1 + element.quantity;
								return true;
							}
						})
						console.log(temp);
						if(temp.length == 0){
							this.list_cart = this.list_cart.concat(itemCv);
						}
						
					//	this.presentToast();

						// this.list_cart = new Array();
					//	this.events.publish('cart_list: change', this.list_cart);
						this.storage.set('cart_list', this.list_cart);
						console.log(this.list_cart);












					}
				});
			});
			}, error => {
		});
		console.log(recomendacao);
	}
	segmentChanged(ev: any) {
		console.log('Segment changed', ev.detail.value);
		this.segment = ev.detail.value;
	  }
	
	ngOnInit() {
	}

}
