import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'shop', pathMatch: 'full' },
  
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  
  { path: 'list', loadChildren: './list/list.module#ListPageModule' },
  // { path: 'list/:cat', loadChildren: './list/list.module#ListPageModule' },

  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'blog', loadChildren: './blog/blog.module#BlogPageModule' },
  
  { path: 'checkout', loadChildren: './checkout/checkout.module#CheckoutPageModule' },
  // { path: 'checkout/:pay', loadChildren: './checkout/checkout.module#CheckoutPageModule' },

  { path: 'detail', loadChildren: './detail/detail.module#DetailPageModule' },
  // { path: 'detail/:obj', loadChildren: './detail/detail.module#DetailPageModule' },

  { path: 'favorites', loadChildren: './favorites/favorites.module#FavoritesPageModule' },
  { path: 'forgot', loadChildren: './forgot/forgot.module#ForgotPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  
  { path: 'mycart', loadChildren: './mycart/mycart.module#MycartPageModule' },

  { path: 'myorder', loadChildren: './myorder/myorder.module#MyorderPageModule' },
  { path: 'offer', loadChildren: './offer/offer.module#OfferPageModule' },
  
  { path: 'post', loadChildren: './post/post.module#PostPageModule' },
  // { path: 'post/:obj', loadChildren: './post/post.module#PostPageModule' },

  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },

  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },

  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'shop', loadChildren: './shop/shop.module#ShopPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
