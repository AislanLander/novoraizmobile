import { Component } from '@angular/core';
import { Platform, Events, MenuController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

import { environment } from '../environments/environment';

// import * as firebase from 'firebase';

import { CurrenciesProvider } from '../providers/currencies';
import { ThemeProvider } from '../providers/theme';
import { UsersProvider } from '../providers/users';

import { tap } from 'rxjs/operators';

import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { finalize } from 'rxjs/operators';

// import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  menu = [];
  user: any;

  constructor(
    public currenciesProv: CurrenciesProvider,
    private themeProvider: ThemeProvider,
    public toastCtrl: ToastController,
    public menuCtrl: MenuController, 
    public events: Events, 
    private storage: Storage, 
    public usersProv: UsersProvider, 
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // private admobFree: AdMobFree
  ) {

    this.menu = environment.menu;



    // const bannerConfig: AdMobFreeBannerConfig = {
    //   id: 'ca-app-pub-3940256099942544/6300978111',
    //   isTesting: true,
    //   autoShow: true
    // };
    // this.admobFree.banner.config(bannerConfig);
    // this.admobFree.banner.prepare().then(() => {}).catch(e => console.log(e));



    this.events.subscribe('user: change', (user) => {
      if(user || user != null){
        console.log('userchange');
        console.log(user);
        this.user = user;
        this.router.navigateByUrl('shop');
        this.menuCtrl.enable(true);
      }else{
        this.router.navigateByUrl('login');
        this.menuCtrl.enable(false);
      }
    });


    this.storage.ready().then(() => {
      this.storage.get('user').then((val) => {
        console.log(val);
        if(val != null){
          this.user = val;
          this.router.navigateByUrl('shop');
          this.menuCtrl.enable(true);
        }else{
          this.router.navigateByUrl('login');
          this.menuCtrl.enable(false);
        }
      })
    })


    // firebase.auth().onAuthStateChanged((user) => {
    //   if(!user){
    //     console.log("not login");
    //     this.menuCtrl.enable(false);
    //     this.router.navigateByUrl('login');
    //   }else{
    //     console.log("login");
    //     storage.ready().then(() => {
    //       storage.get('user').then((val) => {
    //         console.log(val);
    //         this.user = val;
    //         this.menuCtrl.enable(true);
    //         this.router.navigateByUrl('home');
    //       });
    //     })
    //   }
    // });


    this.initializeApp();
  }

  ionViewWillEnter(){

  }


  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg.body,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout() {
    this.usersProv.logoutUser().then(() => {
      this.storage.remove('user');
      this.user = null;
      this.storage.remove('cart_list');
      this.router.navigateByUrl('/login');
      this.menuCtrl.enable(false);
    });
  }

}
