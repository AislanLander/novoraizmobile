import {Http, Response, 
    Headers, RequestOptions}  from '@angular/http'
import { Injectable }             from '@angular/core';
import { Dados }                  from './dados.class';
import { map } from 'rxjs/internal/operators/map';


/* CLASSE SERVIÇO: RESPONSÁVEL POR ESTABELECER COMUNICAÇÃO COM O SERVIDOR */

@Injectable()
export class PagamentoService {
constructor(private http: Http) {
}

public firebaseTest() {
    // Using Angular HttpClient to send POST request
const httpOptions = {
    headers: new Headers({
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
   })
   };
   let data = {name: 'Dale Nguyen'};
  return this.http.post('https://us-central1-raizagrogo.cloudfunctions.net/pagSeguroSessions', data, httpOptions);
}

public firebaseCredito(valor, hash, dados) {
    let valorCompra = Number(valor).toFixed(2);
    console.log(dados);
    console.log('iniciando processamento de cartão' + dados.parcelas[0].quantity);
    const httpOptions = {
        headers: new Headers({
            
        //    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
            'Content-Type' : 'application/json',
        })
    };
    let data = {
        data: `<payment>
        <mode>default</mode>
        <method>creditCard</method>
        <sender>
            <name>`+dados.nome+`</name>
            <email>`+dados.email+`</email>
            <phone>
                <areaCode>11</areaCode>
                <number>30380000</number>
            </phone>
            <documents>
                <document>
                    <type>CPF</type>
                    <value>`+dados.cpf+`</value>
                </document>
            </documents>
            <hash>`+ hash+`</hash>
        </sender>
        <currency>BRL</currency>
        <notificationURL>https://sualoja.com.br/notificacao</notificationURL>
        <items>
            <item>
                <id>1</id>
                <description>Descricao do item a ser vendido</description>
                <quantity>1</quantity>
                <amount>`+valorCompra+`</amount>
            </item>
        </items>
    <extraAmount>0.00</extraAmount>
        <reference>R123456</reference>
    <shippingAddressRequired>true</shippingAddressRequired>
        <shipping>
            <address>
                <street>`+dados.logradouro+`</street>
                <number>`+dados.numero+`</number>
                <complement>1 andar</complement>
                <district>`+dados.bairro+`</district>
                <city>`+dados.cidade+`</city>
                <state>`+dados.estado+`</state>
                <country>BRA</country>
                <postalCode>01452002</postalCode>
            </address>
            <type>3</type>
            <cost>0.00</cost>
        </shipping>
        <creditCard>
            <token>`+dados.hashCard+`</token>
           <installment>
                 <quantity>`+dados.parcelas[0].quantity+`</quantity>
                <value>`+dados.parcelas[0].totalAmount+`</value>
                        <noInterestInstallmentQuantity>3</noInterestInstallmentQuantity>
            </installment>
            <holder>
                <name>`+dados.nome+`</name>
                <documents>
                    <document>
                        <type>CPF</type>
                        <value>`+dados.cpf+`</value>
                    </document>
                </documents>
                <birthDate>`+dados.nascimento+`</birthDate>
                <phone>
                    <areaCode>11</areaCode>
                    <number>999991111</number>
                </phone>
            </holder>
            <billingAddress>
            <street>`+dados.logradouro+`</street>
            <number>`+dados.numero+`</number>
            <complement>1 andar</complement>
            <district>`+dados.bairro+`</district>
            <city>`+dados.cidade+`</city>
            <state>`+dados.estado+`</state>
            <country>BRA</country>
            <postalCode>01452002</postalCode>
            </billingAddress>
        </creditCard>
    </payment>`
    }
    return this.http.post('https://us-central1-raizagrogo.cloudfunctions.net/transacao', data, httpOptions);
}

public firebaseBoleto(valor, hash, dados) {
    let valorCompra = Number(valor).toFixed(2);
    console.log('firebaseBoleto')
    // Using Angular HttpClient to send POST request
    const httpOptions = {
        headers: new Headers({
            
        //    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
            'Content-Type' : 'application/json',
    })
    };
    let data =  {data:`
        <payment>
        <mode>default</mode>
        <method>boleto</method>
        <sender>
            <name>`+dados.nome+`</name>
            <email>`+dados.email+`</email>
            <phone>
                <areaCode>11</areaCode>
                <number>`+dados.telefone+`</number>
            </phone>
            <documents>
                <document>
                    <type>CPF</type>
                    <value>`+dados.cpf+`</value>
                </document>
            </documents>
            <hash>`+hash+`</hash>
        </sender>
        <currency>BRL</currency>
        <notificationURL>https://sualoja.com.br/notificacao</notificationURL>
        <items>
            <item>
                <id>1</id>
                <description>Descricao do item a ser vendido</description>
                <quantity>1</quantity>
                <amount>`+valorCompra+`</amount>
            </item>
        </items>
        <extraAmount>0.00</extraAmount>
        <reference>R123456</reference>
        <shippingAddressRequired>true</shippingAddressRequired>
        <shipping>
            <address>
                <street>`+dados.logadrouro+`</street>
                <number>`+dados.numero+`</number>
                <complement>1 andar</complement>
                <district>`+dados.bairro+`</district>
                <city>`+dados.cidade+`</city>
                <state>`+dados.estado+`</state>
                <country>BRA</country>
                <postalCode>`+dados.cep+`</postalCode>
            </address>
            <type>3</type>
            <cost>0.00</cost>
        </shipping>
        </payment>`};
    return this.http.post('https://us-central1-raizagrogo.cloudfunctions.net/transacao', data, httpOptions);
    }


public store (dados:Dados){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    
    let body = JSON.stringify({ dados });
    return this.http.post('http://www.suaApi.com.br/store', body, options).pipe(
        map(res => res.json())

    ).subscribe(res => console.log(res));
}

public cancel (){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get('http://www.suaApi.com.br/cancel', options).pipe(
        map(res => res.json())

    ).subscribe(res => console.log(res));
}
}
