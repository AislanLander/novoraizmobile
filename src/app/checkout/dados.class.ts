export class Dados {
  public id: number;
  public nome: string = "";
  public telefone: string = "";
  public email: string = "";
  public cpf: string = "";
  public nascimento: string = "";
  public logradouro: string = "";
  public numero: string = "";
  public bairro: string = "";
  public cep: string = "";
  public cidade: string = "";
  public estado: string = "";
  public numCard: "";
  public mesValidadeCard: "";
  public anoValidadeCard: "";
  public codSegCard: "";
  public hashComprador: string; // preenchido dinamicamente
  public bandCard: string; // preenchido dinamicamente
  public hashCard: string; // preenchido dinamicamente
  public parcelas: Array<Object> = []; // preenchido dinamicamente

  constructor(obj?) {
    Object.assign(this, obj, {}, {});
  }
}
