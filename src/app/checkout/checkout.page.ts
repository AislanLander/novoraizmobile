import { Component } from '@angular/core';
import { ToastController, LoadingController, AlertController, Events, Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { environment } from '../../environments/environment';
import { Storage } from '@ionic/storage';
import { Stripe } from '@ionic-native/stripe/ngx';
import { CurrenciesProvider } from '../../providers/currencies';
import { OrdersProvider } from '../../providers/orders';
import { PagamentoService }  from './pagamento.service';
import { Dados }             from './dados.class';
import { VariableGlobal }    from './variable.global.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NgxXml2jsonService } from 'ngx-xml2json';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import * as pagseguro from '../../assets/js/pagseguro.js';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
declare var PagSeguroDirectPayment:any;

@Component({
	selector: 'page-checkout',
	templateUrl: './checkout.page.html',
	styleUrls: ['./checkout.page.scss']
})
export class CheckoutPage {
	public dados = new Dados ();
	letterObj = {
		to: '',
		from: '',
		text: ''
	  }
	  pdfObj = null;

	total_price: any;
	tax_pay: any;
	shipfee_pay: any;
	total_pay: any;
	fullname: string = '';
	phone: string = '';
	address: string = '';
	validate: boolean = true;
	pay_method: any = 1;
	email: string = '';

	user_id: any = '';

	/*this card info just for test, pls set it to empty string when your app go online */
	card_info: any = {
		number: '4242424242424242',
		expMonth: '12',
		expYear: '2030',
		cvc: '200'
	}
	carts: any = '';
	settings: any = '';

	loading: any;

	addressForm : FormGroup;
	sessionPagseguro = '';
	hashCliente = '';
	boletoLink ='';
	boletoObj:any;
	constructor(
		private ngxXml2jsonService: NgxXml2jsonService,
		private fileOpener: FileOpener,
		private route: ActivatedRoute,
		private router: Router,
		public payPal: PayPal,
		public storage: Storage, 
		public toastCtrl: ToastController, 
		public stripe: Stripe,
		public events: Events,
		private formBuilder: FormBuilder,
		public currenciesProv: CurrenciesProvider,
		public ordersProv: OrdersProvider,
		public loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		public pagamentoService: PagamentoService, private variableGlobal: VariableGlobal,
		private file: File, 
		private plt: Platform, 
		) {
	
		this.route.params.subscribe(params => {
			this.total_price = params.total_price;
			this.tax_pay = params.tax_pay;
			this.shipfee_pay = params.shipfee_pay;
			this.total_pay = params.total_pay;
			console.log(this.total_pay);
			this.carregaJavascriptPagseguro();
			this.buscaPagamentos();
		});

		this.pay_method = 1;
		let key: string;
		let xmls : any;
		// testa funcionameto da Api e solicita SessionID do pagseguro
		let res = this.pagamentoService.firebaseTest().subscribe(
			res => {
			  console.log(res)
			  console.log(res.text());
			  key = res.text();
	 		  const parser = new DOMParser();
			  const xml = parser.parseFromString(key, 'text/xml');
			  const obj = this.ngxXml2jsonService.xmlToJson(xml);
			  console.log(obj);
			  xmls = obj;
			  //console.log(xmls.session.id);
				this.sessionPagseguro = xmls.session.id;
			  return res.text();
			},
			err => {
			  console.log("Error occured");
			}
		   );
/* 		console.log(res,key);
		console.log(xmls.session.id); */
		this.pay_method = 1;
		this.storage.get('user').then((obj) => {
			console.log(obj);
			if (obj == null) {
				this.user_id = null;
			}else{
				this.user_id = obj.id_auth;
				this.fullname = obj.fullname;
				this.phone = obj.phone;
				this.email = obj.email;
			}
		});
		
		this.storage.ready().then(() => {
			this.storage.get('cart_list').then(data=>{
				this.carts=data;
			})

			this.storage.get('setting').then(data=>{
				this.settings=data;
			})
		})

		this.addressForm = this.formBuilder.group({
			address: ['', Validators.compose([Validators.minLength(2), Validators.required])],
			message: [''],
			cpf: ['', Validators.compose([Validators.required])]
		});

		this.events.subscribe('user: change', () => {
			this.ionViewWillEnter();
		});

	}
	getUserHash() {
		PagSeguroDirectPayment.setSessionId(this.sessionPagseguro);
		PagSeguroDirectPayment.onSenderHashReady((response) =>{
			if(response.status == 'error') {
				console.log(response.message);
				return false;
			}
			var hash = response.senderHash; //Hash estará disponível nesta variável.
			console.log(hash);
			this.hashCliente = hash;
		});

	}
	  //BUSCA A BANDEIRA DO CARTÃO (EX: VISA, MASTERCARD ETC...) E DEPOIS BUSCA AS PARCELAS;
  //ESTA FUNÇÃO É CHAMADA QUANDO O INPUT QUE RECEBE O NÚMERO DO CARTÃO PERDE O FOCO;
  buscaPagamentos(){
	  // session ID temporario

	PagSeguroDirectPayment.setSessionId(this.sessionPagseguro);
	console.log('buscarpagamentos')
	PagSeguroDirectPayment.getPaymentMethods({
		amount: this.total_pay,
		success: function(response) {
			// Retorna os meios de pagamento disponíveis.
			console.log(response)
		},
		error: function(response) {
			// Callback para chamadas que falharam.
			console.log(response)
		},
		complete: function(response) {
			// Callback para todas chamadas.
			console.log(response)
		}
	});	
  }
  buscaBandeira(){
	PagSeguroDirectPayment.setSessionId(this.sessionPagseguro);
	console.log(this.dados.numCard.substring(0,6))
    PagSeguroDirectPayment.getBrand({
					cardBin: this.dados.numCard.substring(0,6),
					success: response => { 
            
            this.dados.bandCard = response.brand.name;
            this.buscaParcelas();
            console.log('Bandeira do cartão: ' + this.dados.bandCard);
        
          },
					error: response => { console.log(response); }
		});	
 
  }


  //BUSCA AS PARCELAS NA API DO PAGSEGURO PARA O CLIENTE ESCOLHER
  buscaParcelas(){
	PagSeguroDirectPayment.setSessionId(this.sessionPagseguro);

    PagSeguroDirectPayment.getInstallments({

				amount:  this.total_pay,              //valor total da compra (deve ser informado)
				brand: this.dados.bandCard, //bandeira do cartão (capturado na função buscaBandeira)
				maxInstallmentNoInterest: 3, // quantas vezes é aceito pagamento sem juros [?] TODO0
				success: response => { 

          this.dados.parcelas = response.installments[this.dados.bandCard];
		  console.log('parcelas: '+ response.installments[this.dados.bandCard].length + ' vezes com juros');
		  console.log(response)

				},
				error: response => {	console.log( response)	}
		});
		

  }

  onSubmit(f){
	  let boleto :any;
	  console.log(this.sessionPagseguro);
	  this.pagamentoService.firebaseBoleto(this.total_pay, this.hashCliente, this.dados).subscribe(
		res => {
		  console.log(res)
		  console.log(res.text());
		  let key = res.text();
		   const parser = new DOMParser();
		  const xml = parser.parseFromString(key, 'text/xml');
		  const obj = this.ngxXml2jsonService.xmlToJson(xml);
		  console.log(obj);
		  boleto = obj;
		  console.log(boleto.transaction.paymentLink);
		  this.boletoObj = obj;
			this.boletoLink = boleto.transaction.paymentLink;
		  return res.text();
		},
		err => {
		  console.log("Error occured");
		}
	   );
	 // 	PagSeguroDirectPayment.setSessionId('c4ff46f853ac4095a28b5b94fa76b0b2')/
/* 		  PagSeguroDirectPayment.onSenderHashReady(function(response){
			if(response.status == 'error') {
				console.log(response.message);
				return false;
			}
			var hash = response.senderHash; //Hash estará disponível nesta variável.
			console.log(hash);
		});
		 */
}

enviaDadosParaServidor(){

	//COLOQUE AQUI O CÓDIGO PARA ENVIAR OS DADOS PARA O SERVIDOR (PHP, JAVA ETC..) PARA QUE ELE CONSUMA A API DO PAGSEGURO E CONCRETIZE A TRANSAÇÃO
	this.pagamentoService.store(this.dados);
  }
  //CARREGA O JAVASCRIPT DO PAGSEGURO (A EXPLICAÇÃO ESTA FUNÇÃO ESTÁ LOGO ABAIXO)
  carregaJavascriptPagseguro(){
      
	if(!this.variableGlobal.getStatusScript())
	{
	  //SEJA O JAVASCRIPT NO CABEÇÁRIO DA PÁGINA
	  new Promise((resolve) => {
		let script: HTMLScriptElement = document.createElement('script');
		script.addEventListener('load', r => resolve());
		script.src = 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
		document.head.appendChild(script);      
	  });
	  this.variableGlobal.setStatusScript(true);
	}

  }
/*
PARA USAR O CHECKOUT TRANSPARENTE DO PAGSEGURO, É NECESSÁRIO CARREGAR UM ARQUIVO JS EXTERNO (pagseguro.directpayment.js). 
AQUI NÓS USAMOS UM SCRIPT PARA QUE ESSE JS SEJA CARREGADO SOMENTE NA HORA QUE ESTE COMPONENTE FOR CHAMADO (EVITANDO CARREGAR O JS DO PAGSEGURO 
TODA VEZ QUE O COMPONENTE FOR CHAMADO). PORÉM, SE VOCÊ PREFERIR, O JS PODE SER CARREGADO NO INDEX.HTML (FICA AO SEU CRITÉRIO). 
O SCRIPT, QUE FICA NA FUNÇÃO carregaJavascriptPagseguro(), CRIA UMA TAG DO TIPO SCRIPT NO HEAD DA PÁGINA E SETA O ARQUIVO JS PARA EVITAR QUE O JS SEJA 
CARREGADO TODA HORA QUE O COMPONENTE FOR CHAMADO. TAMBÉM CRIAMOS UM SERVIÇO GLOBAL QUE ARMAZENA UMA VARIÁVEL BOOLEANA PARA INFICAR SE O JS JÁ 
FOI CARREGADO OU NÃO. UMA VEZ CARREGADO, O JS NÃO SERÁ CARREGADO NOVAMENTE.
*/

	ionViewWillEnter(){ 
		this.getUserHash();
		this.storage.get('user').then((obj) => {
			console.log(obj);
			if (obj == null) {
				this.user_id = null;
			}else{
				this.user_id = obj.id_auth;
				this.fullname = obj.fullname;
				this.phone = obj.phone;
				this.email = obj.email;
			}
		});
	}

	async presentToast() {
		const toast = await this.toastCtrl.create({
			message: 'Adicionado com sucesso',
			duration: 2000,
			position: 'top'
		});
		toast.present();
	}

	async presentLoading() {
		this.loading = await this.loadingCtrl.create({
			message: 'aguardando',
			duration: 2000
		});
		return await this.loading.present();
	}

	async presentAlertErr(err) {
		const alert = await this.alertCtrl.create({
			message: err,
			buttons: [{
				text: "Ok",
				role: 'cancel'
			}]
		});
		await alert.present();
	}

	order(){
		if (!this.addressForm.valid) {
			this.validate = false;
			console.log(this.addressForm.value);
		}else{
			this.validate = true;
			let data = {
				pay_method: parseInt(this.pay_method),
				email: this.email,
				total_price: this.total_price,
				tax_pay: parseFloat(this.tax_pay),
				shipfee_pay: parseFloat(this.shipfee_pay),
				total_pay: parseFloat(this.total_pay),
				fullname: this.fullname,
				id_user: this.user_id,
				phone: this.phone,
				address: this.addressForm.value.address,
				message: this.addressForm.value.message,
				items: JSON.stringify(this.carts)
			}

			if(this.pay_method==1){
				this.order_on_delivery(data);
			}
			if(this.pay_method==2){
				this.order_card(data);
			}
			if(this.pay_method==3){
				this.onSubmit(`f`);
			}
		}
	}
	

	order_on_delivery(data){
		this.ordersProv.addOrders(data).then(val => {
			this.storage.remove('cart_list').then(() =>{
				this.presentAlertErr('Pedido com sucesso!').then(() => {
					this.router.navigateByUrl('/home');
				});
			});
		})
	}


	order_card(data){
		let resposta: any;
		console.log(this.sessionPagseguro);
		PagSeguroDirectPayment.setSessionId(this.sessionPagseguro);

			//CRIA O HASK DO CARTÃO DE CRÉDITO JUNTO A API DO PAGSEGURO
	PagSeguroDirectPayment.createCardToken({
		/* 
		  cardNumber:       this.dados.numCard,
		  cvv:              this.dados.codSegCard,
		  expirationMonth:  this.dados.mesValidadeCard,
		  expirationYear:   this.dados.anoValidadeCard,
	//	  brand:            this.dados.bandCard,
		  brand:            'visa',*/
		//  hash:  `8509f025c1474270bc1665bc1cf03a07`,
		  cardNumber: '4111111111111111', // Número do cartão de crédito
		  brand: 'visa', // Bandeira do cartão
		  cvv: '013', // CVV do cartão
		  expirationMonth: '12', // Mês da expiração do cartão
		  expirationYear: '2026', // Ano da expiração do cartão, é necessário os 4 dígitos.
		  success: response => {
		  this.dados.hashCard = response.card.token;
		  console.log(this.dados);
		  //NESTE MOMENTO JÁ TEMOS TUDO QUE PRECISAMOS!
		  //HORA DE ENVIAR OS DADOS PARA O SERVIDOR PARA CONCRETIZAR O PAGAMENTO
		  this.pagamentoService.firebaseCredito(this.total_pay, this.hashCliente, this.dados).subscribe(
			res => {
			  console.log(res)
			  console.log(res.text());
			  let key = res.text();
			   const parser = new DOMParser();
			  const xml = parser.parseFromString(key, 'text/xml');
			  const obj = this.ngxXml2jsonService.xmlToJson(xml);
			  console.log(obj);
			  resposta = obj;
			  console.log(resposta);
			  return res.text();
			},
			err => {
			  console.log("Error occured");
			}
		   );
		  },
		  error: response => { console.log(response) }
		});
	  
/* 		this.stripe.setPublishableKey(environment.stripe_publish_key);
		this.stripe.createCardToken(this.card_info).then((token) => {

			this.ordersProv.addOrders(data).then(val => {
				this.storage.remove('cart_list').then(() =>{
					this.presentAlertErr('Pedido com sucesso!').then(() => {
						this.router.navigateByUrl('/home');
					});
				});
			})

		}).catch(error => {
			this.presentAlertErr(error);
		}); */
	}

	order_on_paypal(data){
		this.payPal.init({
			PayPalEnvironmentProduction: environment.paypal_live_client_id,
			PayPalEnvironmentSandbox: environment.paypal_sandbox_client_id
		}).then(() => {
			this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
			})).then(() => {
				let payment = new PayPalPayment(this.total_pay,this.settings.currency_code, 'Buy Pizza', 'sale');
				this.payPal.renderSinglePaymentUI(payment).then(() => {
					
					this.ordersProv.addOrders(data).then(val => {
						this.storage.remove('cart_list').then(() =>{
							this.presentAlertErr('Pedido com sucesso!').then(() => {
								this.router.navigateByUrl('/home');
							});
						});
					})

				}, () => {
				});
			}, () => {
			});
		}, () => {
		});

	}

	
	createPdf() {
		var docDefinition = {
		  content: [
			{ text: 'Contrato', style: 'header' },
			{ text: new Date().getDate() + `/`+ new Date().getMonth() +1+ `/`+  new Date().getFullYear(), alignment: 'right' },
	 
			{ text: 'CÉDULA DE PRODUTO RURAL – CPR – N.º 002, DE 30/01/2009', style: 'subheader' },
			{ text: `Produtor Teste Teste da Silva`},
	 
			{ text: 'CÉDULA DE PRODUTO RURAL – CPR Regulamentada pela Lei n.º 11.775/2008 CPR N.º , DE “DATAHOJE”', style: 'subheader' },
			this.letterObj.to,
	 
			{ text: `CÉDULA DE PRODUTO RURAL – CPR Regulamentada pela Lei n.º 11.775/2008 CPR N.º , DE “DATAHOJE” `, style: 'story', margin: [0, 20, 0, 20] },
			{ text: this.addressForm.value.cpf, style: 'subheader' },

			{
			  ol: [
				'CÉDULA DE PRODUTO RURAL: instituída pelo artigo 15-B da Lei n.º 11.322, de 13/07/2006, alterada pelas Leis n.ºs 11.420, de 20/12/2006, e 11.775, de 17/09/2008, no sentido de firma entrega ao comprador (nome revenda/ CNPJ, endereço) ,aqui nessa CPR mencionado a receber produto (soja ou milho Revenda define) no padrão de qualidade,   data e valor estabelecido aqui nessa CPR.',
				'VOLUME TOTAL: ' + this.total_pay,
				'VENCIMENTO: (pagamento único ou parcelado – definição da revenda)',
				`Pagamento Único em ___/____/______ (ou caso Parcelado) x sacos conforme valor Top 2)
				1.ª parcela em ____ /_____ /_______ de _____________ sacas ou ___________Toneladas
				2.ª parcela em ____/_____ /________ de _____________ Sacas ou __________ Toneladas
				`,
				' QUALIDADE DO PRODUTO: Umidade 14%, impureza 1%, avariado 8% (padrão revenda)',
				'LIQUIDAÇÃO: poderá ser feita em uma das seguintes modalidades, sendo admitida a liquidação antecipada: liquidação financeira, liquidação física, liquidação física e financeira e liquidação física com doação simultânea. No pagamento em produto, emitente/devedor se obriga a comunicar formalmente ao Comprador (nome revenda), no prazo de até 30 dias antes do vencimento de cada parcela, a decisão de entregar o produto in natura ou processado/beneficiado. ',
				'VIGÊNCIA: vigerá até que seja efetuado o pagamento (Único/última parcela) acima, estando resolvido de pleno direito, dando total, plena e irrevogável quitação de qualquer outro encargo ou responsabilidade decorrente da dívida contraída na respectiva CPR. ',
				'PENALIDADE: na hipótese de mora, pelo não pagamento do valor devido, até o vencimento de cada parcela, deverá ser o valor recalculado com os encargos de 2,0% ao ano (dois por cento ao ano), mais 2,0% ao mês de juros. (valor da revenda)',
				'EXEQÜIBILIDADE: o Emitente/Devedor está ciente de que o descumprimento das condições previstas neste aditivo, ensejará a adoção das medidas extrajudiciais ou judiciais cabíveis para a satisfação deste Título, podendo ocorrer ainda, o registro do seu nome no Cadastro Informativo de créditos não quitados dos órgãos e entidades federais – CADIN e também no SIRCOI. ',
				'RATIFICAÇÃO: ficam ratificados todos os demais itens e condições da CPR. Para os devidos e legais efeitos de direito, Emitente/Devedor e Credor, firmam o presente Termo Aditivo em duas vias de igual forma e teor, na presença das testemunhas instrumentárias. ',
				`Cidade (UF),________ de_________ de_____________. 


				_____________________________________
									  EMITENTE/DEVEDOR                               
				 
				_____________________________________
								Testemunha (nome e CPF) 
				
				_____________________________________
								Testemunha (nome e CPF)
				`

			  ]
			}
		  ],
		  styles: {
			header: {
			  fontSize: 18,
			  bold: true,
			},
			subheader: {
			  fontSize: 14,
			  bold: true,
			  margin: [0, 15, 0, 0]
			},
			story: {
			  italic: true,
			  alignment: 'center',
			  width: '50%',
			}
		  }
		}
		this.pdfObj = pdfMake.createPdf(docDefinition);
	  }
	 
	  downloadPdf() {
		if (this.plt.is('cordova')) {
		  this.pdfObj.getBuffer((buffer) => {
			var blob = new Blob([buffer], { type: 'application/pdf' });
	 
			// Save the PDF to the data Directory of our App
			this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
			  // Open the PDf with the correct OS tools
			  this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
			})
		  });
		} else {
		  // On a browser simply use download!
		  this.pdfObj.download();
		}
	  }

}
