import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

import { CategoriesProvider } from '../../providers/categories';

@Component({
	selector: 'app-shop',
	templateUrl: './shop.page.html',
	styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {

	list_cat: any;
	loading: any;

	constructor(
		public catProv: CategoriesProvider,
		public loadingCtrl: LoadingController
	) {
		this.presentLoading();
		this.catProv.getCatParent().then(data => {
			this.loading.dismiss().then(() => {
				this.list_cat = data;
				console.log(this.list_cat);
			});
		}, error => {

		});
	}

	ngOnInit() {
	}

	async presentLoading() {
		this.loading = await this.loadingCtrl.create({
			message: 'aguarde',
			duration: 2000
		});
		return await this.loading.present();
	}

}
