import { NgModule, Component } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { IonicStorageModule } from "@ionic/storage";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

import { Firebase } from "@ionic-native/firebase";
import * as firebase from "firebase";

import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { environment } from "../environments/environment";

import { AboutProvider } from "../providers/about";
import { CategoriesProvider } from "../providers/categories";
import { PostsProvider } from "../providers/posts";
import { ProductsProvider } from "../providers/products";
import { UsersProvider } from "../providers/users";
import { OrdersProvider } from "../providers/orders";
import { ContactsProvider } from "../providers/contacts";
import { SettingsProvider } from "../providers/settings";
import { CurrenciesProvider } from "../providers/currencies";
import { UploadProvider } from "../providers/upload";
import { FcmProvider } from "../providers/fcm";
import { FavoritesProvider } from "../providers/favorites";
import { ThemeProvider } from "../providers/theme";

import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";

import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal/ngx";
import { Stripe } from "@ionic-native/stripe/ngx";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { File } from "@ionic-native/file/ngx";

import { AdMobFree } from "@ionic-native/admob-free/ngx";
import { PagamentoService } from "./checkout/pagamento.service";
import { HttpModule } from "@angular/http";
import { VariableGlobal } from "./checkout/variable.global.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { NativeGeocoder } from "@ionic-native/native-geocoder/ngx";
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [
    Geolocation,
    NativeGeocoder,
    FileOpener,
    VariableGlobal,
    PagamentoService,
    StatusBar,
    SplashScreen,
    AboutProvider,
    CategoriesProvider,
    PostsProvider,
    ProductsProvider,
    UsersProvider,
    OrdersProvider,
    ContactsProvider,
    SettingsProvider,
    CurrenciesProvider,
    UploadProvider,
    FcmProvider,
    FavoritesProvider,
    ThemeProvider,
    Facebook,
    Stripe,
    CallNumber,
    SocialSharing,
    PayPal,
    InAppBrowser,
    Camera,
    AdMobFree,
    File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
